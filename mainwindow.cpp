#include "mainwindow.h"

MainWindow::MainWindow(int c_rd, QWidget *parent) : QMainWindow(parent), first_node{std::nullopt}, second_node{std::nullopt}, circle_rad{c_rd}, node_id{0}, V{0}, E{0}
{
    this->setFixedHeight(500);
    this->setFixedWidth(500);
    wdg = new QWidget(this);
    layout = new QHBoxLayout(wdg);
    layout->setSizeConstraint(QLayout::SetFixedSize);
    find = new QPushButton("Find");
    save = new QPushButton("Save");
    load = new QPushButton("Load");
    reset = new QPushButton("Reset");
    layout->addWidget(find);
    layout->addWidget(save);
    layout->addWidget(load);
    layout->addWidget(reset);
    wdg->setLayout(layout);
    setCentralWidget(wdg);
    connect(find, SIGNAL(clicked()), this, SLOT(handle_find_btn()));
    connect(save, SIGNAL(clicked()), this, SLOT(handle_save_btn()));
    connect(load, SIGNAL(clicked()), this, SLOT(handle_load_btn()));
    connect(reset, SIGNAL(clicked()), this, SLOT(handle_reset_btn()));
}

void MainWindow::paintEvent(QPaintEvent *event)
{

    QPainter painter(this);

    QPen pen;

    std::for_each(nodes_pos.begin(), nodes_pos.end(), [&](const auto &node) {
        pen.setColor(Qt::white);
        pen.setWidth(3);
        painter.setPen(pen);
        painter.drawEllipse(QPoint(node.pos.first, node.pos.second), circle_rad, circle_rad);
        pen.setColor(Qt::red);
        pen.setWidth(5);
        painter.setPen(pen);
        painter.drawText(QPointF(node.pos.first, node.pos.second), node.name);
    });

    std::for_each(graph.begin(), graph.end(), [&](const auto &edge) {
        pen.setColor(Qt::white);
        pen.setWidth(2);
        painter.setPen(pen);
        painter.drawLine(QLineF(edge.fst.pos.first, edge.fst.pos.second, edge.snd.pos.first, edge.snd.pos.second));
        pen.setColor(Qt::green);
        pen.setWidth(17);
        painter.setPen(pen);
        painter.drawText(QPointF((edge.fst.pos.first + edge.snd.pos.first) / 2, (edge.fst.pos.second + edge.snd.pos.second) / 2), QString::number(edge.weight));
    });
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{

    if (event->buttons() == Qt::LeftButton)
    {
        auto node = searchNode({event->x(), event->y()});
        if (!node.has_value())
        {

            this->nodes_pos.push_back({event->x(), event->y(), node_id, QString::number(node_id)});
            node_id++;
        }
        else
        {
            first_node = node;
            if (second_node.has_value())
            {
                if (searchEdge({first_node.value(), second_node.value()}))
                {
                    bool ok = false;
                    int w = QInputDialog::getInt(this, tr("Enter weight of edge"), tr("Weight:"), 0, 0, 100, 1, &ok);
                    if (ok)
                    {
                        graph.push_back({first_node.value(), second_node.value(), w});
                    }
                }

                first_node.reset();
                second_node.reset();
            }
        }
    }
    else if (event->buttons() == Qt::RightButton)
    {
        auto node = searchNode({event->x(), event->y()});
        if (!node.has_value())
        {

            this->nodes_pos.push_back({event->x(), event->y(), node_id, QString::number(node_id)});
            node_id++;
        }

        else
        {
            second_node = node;
            if (first_node.has_value())
            {

                if (searchEdge({first_node.value(), second_node.value()}))
                {
                    bool ok;
                    int w = QInputDialog::getInt(this, tr("Enter weight of edge"), tr("Weight:"), 0, 0, 100, 1, &ok);
                    if (ok)
                    {
                        graph.push_back({first_node.value(), second_node.value(), w});
                    }
                }

                first_node.reset();
                second_node.reset();
            }
        }
    }
    update();
}

std::optional<MainWindow::Node> MainWindow::searchNode(const Node &node) const
{

    auto res = std::find_if(nodes_pos.begin(), nodes_pos.end(), [&](const auto &circle) {
        auto d = pow(node.pos.first - circle.pos.first, 2) + pow(node.pos.second - circle.pos.second, 2);
        return d < pow(circle_rad, 2);
    });

    return nodes_pos.end() == res ? std::nullopt : std::make_optional(*res);
}

bool MainWindow::searchEdge(const MainWindow::Edge &edge) const
{

    return graph.end() == std::find(graph.begin(), graph.end(), edge);
}

void MainWindow::initAdjList()
{

    nodes_pos.squeeze();
    this->V = nodes_pos.size();

    graph.squeeze();
    this->E = graph.size();

    adj = new std::list<std::pair<int, int>>[V];

    for (size_t i = 0; i < E; i++)
    {
        int u = graph[i].fst.id;
        int v = graph[i].snd.id;
        int w = graph[i].weight;
        adj[u].push_back(std::make_pair(v, w));
        adj[v].push_back(std::make_pair(u, w));
    }
}

void MainWindow::shortestPath(int src)
{

    std::set<std::pair<int, int>> setds;

    dist.resize(this->V, INF);

    setds.insert(std::make_pair(0, src));
    dist[src] = 0;

    while (!setds.empty())
    {
        std::pair<int, int> tmp = *(setds.begin());
        setds.erase(setds.begin());

        int u = tmp.second;

        for (auto i = adj[u].begin(); i != adj[u].end(); ++i)
        {

            int v = (*i).first;
            int weight = (*i).second;

            if (dist[v] > dist[u] + weight)
            {
                if (dist[v] != INF)
                    setds.erase(setds.find(std::make_pair(dist[v], v)));

                dist[v] = dist[u] + weight;
                setds.insert(std::make_pair(dist[v], v));
            }
        }
    }

    qDebug() << "Vertex   Distance from Source\n";
    for (size_t i = 0; i < V; ++i)
        qDebug() << i << "\t\t" << dist[i] << "\n";
}

void MainWindow::handle_find_btn()
{

    bool ok = false;

    size_t src = QInputDialog::getInt(this, tr("Enter the source"), tr("Source:"), 0, 0, this->node_id, 1, &ok);

    if (ok && src <= V)
    {
        initAdjList();
        shortestPath(src);
    }
    else
    {
        QMessageBox::critical(this, tr(""), tr("Wrong Source"));
    }
    update();
}

void MainWindow::handle_save_btn()
{

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save graph"), "", tr(""));
    if (fileName.isEmpty())
        return;

    QFile file(fileName);

    if (!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::information(this, tr("Unable to open file"), file.errorString());
        return;
    }

    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_4_5);
    graph.squeeze();
    out << graph.size();
    std::for_each(graph.begin(), graph.end(), [&out](const auto &edge) {
        out << edge;
    });
    file.close();
}

void MainWindow::handle_load_btn()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open graph"), "");

    if (fileName.isEmpty())
        return;

    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, tr("Unable to open file"),
                                 file.errorString());
        return;
    }

    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_4_5);

    graph.clear();
    nodes_pos.clear();

    int size;
    QSet<MainWindow::Node> uniq_nodes;
    in >> size;

    for (auto i = 0; i < size; i++)
    {
        MainWindow::Edge edge;
        in >> edge;

        graph.push_back(edge);
        uniq_nodes.insert(edge.fst);
        uniq_nodes.insert(edge.snd);
    }

    std::for_each(uniq_nodes.begin(), uniq_nodes.end(), [this](const auto &node) {
        nodes_pos.push_back(node);
    });

    auto max = std::max_element(nodes_pos.begin(), nodes_pos.end(), [](const auto &a, const auto &b) {
        return a.id < b.id;
    });

    this->node_id = max->id + 1;

    file.close();
    update();
}
void MainWindow::handle_reset_btn()
{

    graph.clear();
    nodes_pos.clear();
    dist.clear();
    first_node.reset();
    second_node.reset();
    this->node_id = this->V = this->E = 0;
    delete[] adj;

    update();
}

MainWindow::~MainWindow()
{
    delete find;
    delete save;
    delete load;
    delete reset;
    delete layout;
    delete wdg;
    delete[] adj;
}
QDataStream &operator<<(QDataStream &out, const MainWindow::Node &node)
{
    out << node.pos
        << node.id
        << node.name;
    return out;
}
QDataStream &operator>>(QDataStream &in, MainWindow::Node &node)
{
    in >> node.pos;
    in >> node.id;
    in >> node.name;
    return in;
}

QDataStream &operator<<(QDataStream &out, const MainWindow::Edge &edge)
{
    out << edge.fst
        << edge.snd
        << edge.weight;
    return out;
}
QDataStream &operator>>(QDataStream &in, MainWindow::Edge &edge)
{
    in >> edge.fst;
    in >> edge.snd;
    in >> edge.weight;
    return in;
}
inline uint qHash(const MainWindow::Node &node)
{
    return qHash(node.name, 0xa03f); // arbitrary value
}