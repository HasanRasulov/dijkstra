#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
#include <QVector>
#include <QDir>
#include <QPair>
#include <QSet>
#include <QPushButton>
#include <QWidget>
#include <QInputDialog>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QFile>
#include <QDataStream>
#include <QFileDialog>
#include <algorithm>
#include <cmath>
#include <optional>
#include <list>
#include <utility>
#include <set>
#define INF 0x3f3f3f3f
namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  struct Node
  {

  public:
    QPair<int, int> pos;
    QString name;
    int id;
    Node() = default;
    Node(int x, int y, int id = 0, QString name = "") : pos{x, y}, name{name}, id{id} {}

    bool operator==(const Node &node) const
    {
      return this->pos == node.pos;
    }
    friend QDataStream &operator>>(QDataStream &in, Node &node);
    friend QDataStream &operator<<(QDataStream &out, const Node &edge);
  };

  struct Edge
  {
  public:
    Node fst;
    Node snd;
    int weight;

    Edge() = default;
    Edge(Node fst, Node snd, int w = 0) : fst{fst}, snd{snd}, weight{w} {}

    bool operator==(const Edge &edge) const
    {
      return (this->fst == edge.fst && this->snd == edge.snd) || (this->snd == edge.fst && this->fst == edge.snd);
    }
    friend QDataStream &operator<<(QDataStream &out, const Edge &edge);
    friend QDataStream &operator>>(QDataStream &in, Edge &edge);
  };

  explicit MainWindow(int c_rd = 17, QWidget *parent = 0);
  ~MainWindow();

  virtual void mousePressEvent(QMouseEvent *);
  virtual void paintEvent(QPaintEvent *);
  void shortestPath(int);

private:
  QVector<Node> nodes_pos;
  std::optional<Node> first_node;
  std::optional<Node> second_node;
  QVector<Edge> graph;
  int circle_rad;
  int node_id;
  std::list<std::pair<int, int>> *adj;
  size_t V;
  size_t E;
  std::vector<int> dist;
  QWidget *wdg;
  QPushButton *find;
  QPushButton *save;
  QPushButton *load;
  QPushButton *reset;
  QHBoxLayout *layout;

private:
  void initAdjList();
  std::optional<Node> searchNode(const Node &) const;
  bool searchEdge(const Edge &) const;
private slots:
  void handle_find_btn();
  void handle_save_btn();
  void handle_load_btn();
  void handle_reset_btn();
};

#endif
